import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <div>
                <div className="w3-container">
                    <img src="https://lh3.googleusercontent.com/proxy/lbjOmIS0PBh0_e-JCSQ7wuKFSunv8ouSgwsRNXB6Z9zIHGcnOgdcTTkF-M_ymWc24tDS2VFlJeszcCi98GYZ4g6AaGB3pkIj4vMGY16q1O9gGcgHi7VZSQnLWCv7XV024oHjM6_JpKg" alt=" " className="w3-image" style={{width: '100%'}} />
                </div>
               
                <div className="w3-center w3-black w3-padding-16">
                    <div className="w3-xlarge w3-section">
                        <i className="fa fa-facebook-official w3-hover-opacity" />
                        <i className="fa fa-instagram w3-hover-opacity" />
                        <i className="fa fa-snapchat w3-hover-opacity" />
                        <i className="fa fa-pinterest-p w3-hover-opacity" />
                        <i className="fa fa-twitter w3-hover-opacity" />
                        <i className="fa fa-linkedin w3-hover-opacity" />
                    </div>
                    <p> TRUNG TÂM TIN HỌC ĐẠI HỌC SƯ PHẠM <a href="http://www.thsp.edu.vn/"  title="W3.CSS" className="w3-hover-text-green">TTSP</a></p>
                </div>
            </div>
        );
    }
}

export default Footer;