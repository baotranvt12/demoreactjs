import React, { Component } from 'react';
import book from '../Header/Header';


class Detail extends Component {
    render() {
       
        
        var pid = parseInt(this.props.computedMatch.useParams.id,10);

        return (
            <div>
            {
                book.map((val,key) => {
                    if (val.id === pid){
                        return <div key={key}> <h5 className="w3-center w3-padding-64"><span className="w3-tag w3-wide">{val.name}</span></h5> </div>
                    }
                    return ' ';
                })
            }   
            </div>
        );
    }
}

export default Detail;