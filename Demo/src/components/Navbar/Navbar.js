import React, { Component } from 'react';


class Navbar extends Component {

    render() {
        return (
            <div>
                <div className="w3-top">
                    <div className="w3-bar w3-white w3-wide w3-padding w3-card">
                    <a href="/" className="w3-bar-item w3-button"><b>R&D</b> Trung Tâm Tin Học ĐH Sư Phạm </a>
                    
                    <div className="w3-right w3-hide-small">
                        <a href="/" className="w3-bar-item w3-button">Trang chủ</a>
                        <a href="about" className="w3-bar-item w3-button">Giới thiệu</a>
                        <a href="contact" className="w3-bar-item w3-button">Liên hệ</a>
                    </div>
                    </div>
                </div>
              
            </div>
        );
    }
}

export default Navbar;
