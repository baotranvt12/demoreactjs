import React, { Component } from 'react';
import {Link} from 'react-router-dom';
class Card extends Component {
 
    to_slug = (str) => {
                    // Chuyển hết sang chữ thường
                    str = str.toString();
                    // xóa dấu
                    str = str.replace(/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g, 'a');
                    str = str.replace(/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g, 'e');
                    str = str.replace(/(ì|í|ị|ỉ|ĩ)/g, 'i');
                    str = str.replace(/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g, 'o');
                    str = str.replace(/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g, 'u');
                    str = str.replace(/(ỳ|ý|ỵ|ỷ|ỹ)/g, 'y');
                    str = str.replace(/(đ)/g, 'd');
                
                    // Xóa ký tự đặc biệt
                    str = str.replace(/([^0-9a-z-\s])/g, '');
                
                    // Xóa khoảng trắng thay bằng ký tự -
                    str = str.replace(/(\s+)/g, '-');
                
                    // xóa phần dự - ở đầu
                    str = str.replace(/^-+/g, '');
                
                    // xóa phần dư - ở cuối
                    str = str.replace(/-+$/g, '');
                
                    // return
                    return str;
    }

    constructor(props) {
        super(props);
        this.state = {
            editing : false
        };
    }
    buttonEdit = () => {
        this.setState( {editing : true});
    }
    buttonSave = () => {
        this.setState( {editing : false});
        this.props.edit(this.props.index, this.txtName.value);
    }   
    buttonDelete = () => {
        this.props.delete(this.props.index);
    }
    renderNormal = () => {
        return <div>
            <button className="w3-button w3-black w3-section" type="submit" onClick={ () => this.buttonEdit()}>
                <i className="fa fa-paper-plane" /> Chỉnh sửa
            </button>
            <button className="w3-button w3-black w3-section" type="submit" onClick={ () => this.buttonDelete()} >
                <i className="fa fa-trash-o" /> Xóa
            </button>
        </div>
      
    }

    renderFrom = () => {
        return <div>
            <form >
                <label htmlFor="fname">Nhập nội dung chỉnh sửa </label><br />
                <input ref = {(input) => {this.txtName = input } } defaultValue={this.props.children}/> <br />
                <button className="w3-button w3-black w3-section" type="submit" onClick={ () => this.buttonSave()} >
                    <i className="fa fa-paper-plane" /> Lưu
                </button>
            </form>
        </div>
    }
    show_button = () => {
        if(this.state.editing === false){
            return this.renderNormal();
        }
        else {
            return this.renderFrom();
        }
    }
       
       
    render() {
        return (
            
            <div className="w3-col l3 m6 w3-margin-bottom">
            <div className="w3-display-container">
                        <div className="w3-display-topleft w3-black w3-padding"> 
                            {this.props.children}
                        </div>
                        <img src={this.props.images} alt=" " style={{width: '100%'}} />
            </div>
            
            {this.show_button()}        
            <p><button className="w3-button w3-black w3-round-xxlarge" >
              <Link className="btn btn-success" to={ "detail/" + this.props.pid + "/" + this.to_slug(this.props.children) + ".html" } >  Xem chi tiết  </Link>
           </button></p>
        </div>   
        );
    }
}

export default Card;