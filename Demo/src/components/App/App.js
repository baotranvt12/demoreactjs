import './App.css';
import React from 'react'
import { Component } from 'react';
import Footer from '../Footer/Footer';
import Routerurl from '../RouterURL/Routerurl';
import Navbar from '../Navbar/Navbar';

class App extends Component {
 
  render(){
    return (
      <div className="App">
            <div className="container"> 
              <Routerurl> </Routerurl> 
              <Navbar/> 
            </div>
            <Footer />
      </div>
    );
  }
}

export default App;
