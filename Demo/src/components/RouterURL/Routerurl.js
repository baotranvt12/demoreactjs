import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    //Switch,
    Route,
    //Link
  } from 'react-router-dom';
//import Card from '../Card/Card';
import Contact from '../Contact/Contact';
import Detail from '../Detail/Detail';
import Header from '../Header/Header';
import Navbar from '../Navbar/Navbar';

class Routerurl extends Component {
    render() {
        return (
            <Router> 
                <div>
                    <Route exact path="/"> <Header /> </Route>
                   
                    <Route path="/contact">
                        <Contact />
                    </Route> 
                    <Route path="/about">
                        <Navbar />
                    </Route> 
                    <Route path="/detail/:id/:slug.html">
                        <Detail />
                    </Route> 
                </div>
            </Router>
    
            
        );
    }
}

export default Routerurl;