import React, { Component } from 'react';
import Card from '../Card/Card';
class Header extends Component {
   
    constructor(props) {
        super(props);
        this.state = {
          book : [
            {
              id : "0",
              name : "Sách Hướng Dẫn Tin Học lớp 6",
              images : "https://cf.shopee.vn/file/0301d43e30d2922d8f3b506584dc7189",
            },
            {
              id : "1",
              name : "Sách Hướng Dẫn Tin Học lớp 7",
              images : "https://cf.shopee.vn/file/8da6190f4053d94ea618a8fd06131e7d",
            },
            {
              id : "2",
              name : "Sách Hướng Dẫn Tin Học lớp 8",
              images : "https://cf.shopee.vn/file/1b749d2b27d5cdae8134927fd187a3a4",
            },
            {
              id : "3",
              name : "Sách Hướng Dẫn Tin Học lớp 9",
              images : "https://cf.shopee.vn/file/3a7b03f6c36e6beb71bd55636137bb0f",
            },
          ]
        };
      }
      delete_book = (id) => {
        var arrBook = this.state.book;
        arrBook.splice(id,1);
        this.setState({book : arrBook});
        console.log("Xóa sản phẩm có ID: " + id);
      }
      editName_book = (id, name) => {
        var arrBook = this.state.book;
        arrBook[id].name = name;
        this.setState({book : arrBook});
        console.log("Cập nhật tên sản phẩm có ID : " + id + " có tên " + name);
      }
      show_book = () => {
        const listBook = this.state.book.map((item, index) =>
        <Card key = {index} index = {index} images={ item.images} pid={item.id}  edit={(id, name) => {this.editName_book(id, name)} } delete={(id) => {this.delete_book(id)} }> { item.name} </Card>
        );
        return listBook
      }

    render() {
        return (
            <div>
                <div className="w3-display-container w3-content w3-wide" style={{maxWidth: '1500px'}} id="home">
                    <img src="https://i.pinimg.com/originals/0b/52/cf/0b52cfa20534e151d7c781034eb22588.jpg" alt="Architecture" width={1500} height={800} />
                    <div className="w3-display-middle w3-margin-top w3-center">
                    <h1 className="w3-xxlarge w3-text-white"><span className="w3-padding w3-black w3-opacity-min"><b>Nguyễn Ngọc Bảo Trân</b></span> <span className="w3-hide-small w3-text-light-grey"></span></h1>
                    </div>
                </div>
                <div className="w3-container w3-padding-32" id="projects">
                     <h3 className="w3-border-bottom w3-border-light-grey w3-padding-16">KỆ SÁCH TIN HỌC</h3>
                </div>
                { this.show_book() } 
            </div>
           
        );
    }
}

export default Header;
